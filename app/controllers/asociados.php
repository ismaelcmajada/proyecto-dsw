<?php
 include "utils/utils.php";
 include "exceptions/FileException.php";
 include "utils/File.php";
 include "entity/Asociado.php";
 
 if ($_SERVER["REQUEST_METHOD"]==="POST") {

    $errores = [];

    if(empty($_POST["nombre"])) {
        array_push($errores,"El campo nombre es obligatorio");
    }

    try {
        
            $nombre = trim(htmlspecialchars($_POST["nombre"]));
            $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        
            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

            $imagen = new File("logo", $tiposAceptados);

            if (count($errores)==0) {
                $imagen->moveUploadFile(Asociado::RUTA_IMAGENES_ASOCIADOS);
            }

            $logo = Asociado::RUTA_IMAGENES_ASOCIADOS.$imagen->getFilename();

            array_push(Asociado::$asociados, new Asociado($nombre, $logo, $descripcion));

            $mensaje = "Datos enviados";
    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }

    echo "<p>Nombre: ".($nombre ?? "")."</p>";
    echo "<p>Logo: ".($logo ?? "")."</p>";
    echo "<P>Descripcion: ".($descripcion ?? "")."</p>";

}

 require __DIR__ ."/../views/asociados.view.php";

?>
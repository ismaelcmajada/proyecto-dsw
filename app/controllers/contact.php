<?php

 $errores = [];
 if($_SERVER['REQUEST_METHOD']==='POST') {

    if(empty($_POST["Nombre"])) {
        array_push($errores,"El campo name es obligatorio");
    }

    if(empty($_POST["Asunto"])) {
        array_push($errores,"El campo subject es obligatorio");
    }

    if(empty($_POST["Email"])) {
        array_push($errores,"El campo email es obligatorio");
    } else if((filter_var($_POST["Email"],FILTER_VALIDATE_EMAIL))==false) {
        array_push($errores,"El campo email no es válido");
    }

}


 include "utils/utils.php";
 require __DIR__ ."/../views/contact.view.php";

?>
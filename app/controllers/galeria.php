<?php
 require "utils/utils.php";
 require "utils/File.php";
 require "entity/ImagenGaleria.php";
 require "entity/Categoria.php";
 require "repository/ImagenGaleriaRepository.php";
 require "repository/CategoriaRepository.php";
 
 
 $descripcion="";
 $imagen="";

    try {

        $pdo = App::getConnection();

        $imagenGaleriaRepository = new ImagenGaleriaRepository(); 
        $categoriaRepository = new CategoriaRepository(); 

        $imagenes = $imagenGaleriaRepository->findAll();
        $categorias = $categoriaRepository->findAll();

        if ($_SERVER["REQUEST_METHOD"]==="POST") {
            $categoria = trim(htmlspecialchars($_POST["categoria"]));
            $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

            $imagen = new File("imagen", $tiposAceptados);

            $imagen->moveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);

            $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

            $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

            $imagenGaleriaRepository->save($imagenGaleria);

            $mensaje = "Se ha guardado la imagen en la BBDD.";
        }
       
        
    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    } catch (QueryException $queryException) {

        $errores [] = $queryException->getMessage();
        
    } catch (AppException $appException) {

        $errores [] = $appException->getMessage();
        
    }


 require __DIR__ ."/../views/galeria.view.php";

 

?>
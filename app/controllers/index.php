<?php
 require "entity/ImagenGaleria.php";
 require "entity/Asociado.php";
 require "utils/utils.php";
 require "repository/ImagenGaleriaRepository.php";
 
$asociadosMostrar;

 if(count(Asociado::$asociados)<=3) {
   $asociadosMostrar=Asociado::$asociados;
 } else {
   $asociadosMostrar=extraerAsociados(Asociado::$asociados);
 }

 try {

  $pdo = App::getConnection();

  $imagenGaleriaRepository = new ImagenGaleriaRepository(); 

  $imagenes = $imagenGaleriaRepository->findAll();

} catch (FileException $fileException) {

  $errores [] = $fileException->getMessage();

} catch (QueryException $queryException) {

  $errores [] = $queryException->getMessage();
  
} catch (AppException $appException) {

  $errores [] = $appException->getMessage();
  
}

 require __DIR__ ."/../views/index.view.php";

?>
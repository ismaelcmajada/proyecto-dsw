<?php

return [

    "proyecto" => "app/controllers/index.php",

    "proyecto/home" => "app/controllers/index.php",
   
    "proyecto/about" => "app/controllers/about.php",
   
    "proyecto/asociados" => "app/controllers/asociados.php",
   
    "proyecto/blog" => "app/controllers/blog.php",
   
    "proyecto/contact" => "app/controllers/contact.php",
   
    "proyecto/galeria" => "app/controllers/galeria.php",
   
    "proyecto/post" => "app/controllers/single_post.php"
   
   ]

?>
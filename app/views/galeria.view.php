<?php
include __DIR__ . "/partials/inicio-doc.partial.php";
include __DIR__ . "/partials/nav.partial.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if (empty($errores)) { ?>
                <p><?= $mensaje ?></p>
                <?php } else { ?>
                <ul>
                    <?php foreach ($errores as $error) { ?>
                    <li><?= $error ?></li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
            <?php } ?>

            <form class="form-horizontal" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Categoría</label>
                        <select class="form-control" name="categoria">

                            <?php foreach ($categorias as $categoria) : ?>

                            <option value="<?= $categoria->getId()?>"><?= $categoria->getNombre();?></option>

                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
            <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Visualizaciones</th>
                    <th scope="col">Likes</th>
                    <th scope="col">Descargas</th>
                    <th scope="col">Categoria</th>
                </tr>
            <thead>
            <tbody>
            <?php
                foreach ($imagenes ?? [] as $imagen) {
                    ?>
                <tr>
                    <th scope="row"><?=$imagen->getId()?></th>
                    <td><?=$imagen->getNombre()?></td>
                    <td><?=$imagen->getDescripcion()?></td>
                    <td><?=$imagen->getNumVisualizaciones()?></td>
                    <td><?=$imagen->getNumLikes()?></td>
                    <td><?=$imagen->getNumDownloads()?></td>
                    <td><?=$imagenGaleriaRepository->getCategoria($imagen)->getNombre()?></td>
                </tr>
            <?php
                }
            ?>
            </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.partial.php"; ?>
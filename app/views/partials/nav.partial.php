<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class="<?php if(active("home")) {echo "active";} ?> lien"><a href="home"><i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class="<?php if(active("about")) {echo "active";} ?> lien"><a href="about"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class="<?php if(active("blog")) {echo "active";} ?> lien"><a href="blog"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class="<?php if(active("contact")) {echo "active";} ?> lien"><a href="contact"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class="<?php if(active("galeria")) {echo "active";} ?> lien"><a href="galeria"><i class="fa fa-image sr-icons"></i> Gallery</a></li>
              <li class="<?php if(active("asociados")) {echo "active";} ?>"><a href="asociados"><i class="fa fa-hand-o-right sr-icons"></i> Asociados</a></li>
            </ul>
         </div>
     </div>
   </nav>
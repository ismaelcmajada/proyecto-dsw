<?php

require_once __DIR__ . "/../core/App.php";
require_once __DIR__ . "/../exceptions/AppException.php";

class Connection
{
    public static function make()
    {

        $config = App::get("config")["database"];

        try {

            $connection = new PDO($config["connection"].';dbname='.$config["name"], $config["username"], $config["password"], $config["options"]);
        } catch (PDOException $PDOException) {
            throw new AppException("No se ha podido conectar con la BBDD");
        }
        return $connection;
    }
}
?>
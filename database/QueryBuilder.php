<?php

require_once __DIR__ . "/../core/App.php";
require_once __DIR__ . "/../exceptions/QueryException.php";
require_once __DIR__ . "/../database/IEntity.php";

abstract class QueryBuilder
{
    private $connection;
    private $table;
    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    //El método findAll recibe como parámetro, la tabla y la clase, a partir de ahí, devuelve crea una consulta y
    //devuelve todas las tuplas de la tabla como objetos de dicha clase.

    public function findAll()
    {
        $sql = "SELECT * FROM $this->table";

        $result = $this->executeQuery($sql);

        if(empty($result)) {
            throw new NotFoundException("No se han encontrado elementos.");
        }

        return $result;

        //return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
        //FETCH_CLASS permite almacenar las tuplas de una consulta, como objetos de una clase, tienen que tener los mismos
        //campos y en el mismo orden.

        //FETCH_PROPS_LATE hace que se llame al constructor antes de que las valores de las columnas sean asignados
        //a las propiedades de la clase.
    }

    public function save(IEntity $entity): void
    {
        try {
            $parameters = $entity->toArray(); //Guardamos en una variable un array asociativo que contiene las propiedades de la entidad pasada por parámetro.

            $sql = sprintf(
                "insert into %s (%s) values (%s)",
                $this->table,
                implode(", ", array_keys($parameters)),
                ":". implode(", :", array_keys($parameters)) //Creamos una sentencia preparada que mediante la función implode, define los campos dentro de la sentencia, usando el array .
            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters); //Ejecutamos la sentencia, insertando los datos.
        } catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD."); //En caso de que haya un error en la sentencia o en la inserción, saltará una excepción.
        }
    }

    public function executeQuery(string $sql): array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if($pdoStatement->execute()===false) {
            throw new QueryException("No se ha podido ejecutar la consulta");
        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    public function find (int $id): IEntity
    {
        $sql = "SELECT * FROM $this->table WHERE id = $id";

        $result = $this->executeQuery($sql);

        if(empty($result)) {
            throw new NotFoundException("No se ha encontrado el elemento con id $id");
        }

        return $result[0];
    }

    
}

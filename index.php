<?php
require "core/bootstrap.php"; //Importamos bootstrap.php, el cual contiene todos los requires necesarios, incluida la configuración de la base de datos.

$routes = require "app/routes.php"; // Importamos las rutas


require $routes[Request::uri()]; //Cogemos del array la uri requerida, usando el método uri de la clase Request
?>
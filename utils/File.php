<?php

    require_once __DIR__ . "/../exceptions/FileException.php";

    class File {
        private $file;

        private $fileName;

        public function __construct(string $fileName, array $arrayTypes) {
            $this->file = $_FILES[$fileName];
            $this->fileName = $this->file["name"];

            if($this->file["name"] == "") {
                throw new FileException("No has enviado el archivo");
            }

            if($this->file["error"] !== UPLOAD_ERR_OK) {
                switch ($this->file["error"]) {
                    case UPLOAD_ERR_INI_SIZE:
                    
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new FileException("Error de tamaño");

                    case UPLOAD_ERR_PARTIAL:
                        throw new FileException("Error de archivo incompleto");
                    default:
                        throw new FileException("Error genérico en la subida del archivo.");
                        break;
                }
            }

            if(in_array($this->file["type"], $arrayTypes)===false) {
                throw new FileException("Error de tipo de archivo");
            }
        }

        /**
         * Get the value of fileName
         */ 
        public function getFileName()
        {
            return $this->fileName;
        }

        public function moveUploadFile(string $ruta)
        {
            if (!is_uploaded_file($this->file["tmp_name"])) {
                throw new FileException("Error, el archivo no se ha subido mediante un formulario.");
            } 
            if (is_file($ruta.$this->file["name"])) {
                $this->file["name"] = time()."_".$this->file["name"];
                $this->fileName = $this->file["name"];

            } 
            if (!move_uploaded_file($this->file["tmp_name"], $ruta.$this->file["name"])) {
                throw new FileException("Error, no se ha podido mover el archivo");
            }
        }

        public function copyFile(string $origen, string $destino)
        {
            if (!is_file($origen.$this->file["name"])) {
                throw new FileException("Error, No existe el fichero ".$origen.$this->file["name"]);
            }

            if (is_file($destino.$this->file["name"])) {
                throw new FileException("Error, El fichero ".$destino.$this->file["name"]." ya existe.");
            }

            if (!copy($origen.$this->file["name"],$destino.$this->file["name"])) {
                throw new FileException("Error, No se ha podido copiar el fichero.");
            }
        }
    }
?>